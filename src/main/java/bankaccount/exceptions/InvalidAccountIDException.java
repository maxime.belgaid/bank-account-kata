package bankaccount.exceptions;

public class InvalidAccountIDException extends Exception {
    public InvalidAccountIDException(String message) {
        super(message);
    }
}
