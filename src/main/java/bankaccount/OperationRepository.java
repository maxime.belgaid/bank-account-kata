package bankaccount;

import bankaccount.exceptions.InvalidAccountIDException;
import bankaccount.exceptions.InvalidAmountException;

import java.util.List;
import java.util.Optional;

public interface OperationRepository {
    Operation add(String accountID, Operation operation) throws InvalidAccountIDException, InvalidAmountException;

    Optional<Operation> findLast(String accountID) throws InvalidAccountIDException;

    List<Operation> findAll(String accountID) throws InvalidAccountIDException;
}
