package bankaccount.printer;

import bankaccount.Operation;
import bankaccount.OperationType;
import bankaccount.OperationsFormatter;

import java.time.format.DateTimeFormatter;
import java.util.List;

public class TabularOperationsFormatter implements OperationsFormatter {

    public static final String AMOUNT_FORMAT = "%s€";
    public static final String NEGATIVE_AMOUNT_FORMAT = "-%s€";
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/dd");

    @Override
    public String format(List<Operation> operations) {
        final var columnWidth = 20;
        final var operationsTable = new StringBuilder(String.format("|%" + columnWidth + "s |%" + columnWidth + "s |%" + columnWidth + "s |%" + columnWidth + "s |%n", "Date", "Operation", "Details", "Account Balance"));
        final var operationFormat = "|%" + columnWidth + "s |%" + columnWidth + "s |%" + columnWidth + "s |%" + columnWidth + "s |%n";
        String date, amount, newBalance;
        if (operations.size() == 0){
           operationsTable.append("No operation registered yet");
        } else {
            for (var operation : operations) {
                date = operation.getDate().format(DATE_FORMATTER);
                amount = getAmountFormat(operation.getType(), operation.getAmount().toPlainString());
                newBalance = getAmountFormat(operation.getType(), operation.getNewBalance().toPlainString());
                operationsTable.append(String.format(operationFormat, date, operation.getType().toString(), amount, newBalance));
            }
        }
        return operationsTable.toString();
    }

    private String getAmountFormat(OperationType operationType, String amount) {
        return operationType == OperationType.WITHDRAWAL ?
                String.format(NEGATIVE_AMOUNT_FORMAT, amount) :
                String.format(AMOUNT_FORMAT, amount);
    }
}
