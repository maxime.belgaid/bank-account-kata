package bankaccount.printer;

import bankaccount.OperationsPrinter;

import java.io.PrintStream;

public class PrintStreamOperationsPrinter implements OperationsPrinter {

    private final PrintStream printStream;

    public PrintStreamOperationsPrinter(PrintStream printStream) {
        this.printStream = printStream;
    }

    @Override
    public void printOperations(String formattedOperations) {
        printStream.println(formattedOperations);
    }
}
