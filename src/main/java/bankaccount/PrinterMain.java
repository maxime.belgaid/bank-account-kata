package bankaccount;

import bankaccount.printer.PrintStreamOperationsPrinter;
import bankaccount.printer.TabularOperationsFormatter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class PrinterMain {

    private static PrintStreamOperationsPrinter printer;

    public static void main(String[] args) {
        printer = new PrintStreamOperationsPrinter(System.out);

        List<Operation> operations = List.of(
                Operation.create(OperationType.DEPOSIT, LocalDateTime.now(), "1", BigDecimal.valueOf(10), BigDecimal.valueOf(100)),
                Operation.create(OperationType.WITHDRAWAL, LocalDateTime.now(), "2", BigDecimal.valueOf(5555.4879), BigDecimal.valueOf(100)),
                Operation.create(OperationType.DEPOSIT, LocalDateTime.now(), "3", BigDecimal.valueOf(88888.32147), BigDecimal.valueOf(100)));

        printer.printOperations(new TabularOperationsFormatter().format(operations));
    }
}
