package bankaccount;

import bankaccount.exceptions.InvalidAccountIDException;
import bankaccount.exceptions.InvalidAmountException;

import java.math.BigDecimal;

public interface AccountService {

    Operation deposit(String accountID, BigDecimal amount) throws InvalidAccountIDException, InvalidAmountException;

    Operation withdraw(String accountID, BigDecimal amount) throws InvalidAccountIDException, InvalidAmountException;

    void printStatement(String accountID, OperationsFormatter formatter, OperationsPrinter printer) throws InvalidAccountIDException;
}
