package bankaccount;

public interface OperationsPrinter {
    void printOperations(String formattedOperations);
}
