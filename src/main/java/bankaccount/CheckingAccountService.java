package bankaccount;

import bankaccount.exceptions.InvalidAccountIDException;
import bankaccount.exceptions.InvalidAmountException;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class CheckingAccountService implements AccountService {

    private final OperationRepository operationRepository;
    private final Clock clock;

    public CheckingAccountService(OperationRepository operationRepository, Clock clock) {
        this.operationRepository = operationRepository;
        this.clock = clock;
    }

    @Override
    public Operation deposit(String accountID, BigDecimal amount) throws InvalidAccountIDException, InvalidAmountException {
        final var previousBalance = getPreviousBalance(accountID);
        return operationRepository.add(accountID, Operation.create(OperationType.DEPOSIT, LocalDateTime.now(clock.withZone(ZoneId.of("UTC"))), accountID, amount, previousBalance.add(amount)));
    }

    @Override
    public Operation withdraw(String accountID, BigDecimal amount) throws InvalidAccountIDException, InvalidAmountException {
        final var previousBalance = getPreviousBalance(accountID);
        return operationRepository.add(accountID, Operation.create(OperationType.WITHDRAWAL, LocalDateTime.now(clock.withZone(ZoneId.of("UTC"))), accountID, amount, previousBalance.subtract(amount)));
    }

    @Override
    public void printStatement(String accountID, OperationsFormatter formatter, OperationsPrinter printer) throws InvalidAccountIDException {
        printer.printOperations(formatter.format(operationRepository.findAll(accountID)));
    }

    private BigDecimal getPreviousBalance(String accountID) throws InvalidAccountIDException {
        return operationRepository.findLast(accountID).map(Operation::getNewBalance).orElse(BigDecimal.ZERO);
    }
}
