package bankaccount;

public enum OperationType {
    WITHDRAWAL, DEPOSIT
}
