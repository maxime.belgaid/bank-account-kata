package bankaccount;

import java.util.List;

public interface OperationsFormatter {
    String format(List<Operation> operations);
}
