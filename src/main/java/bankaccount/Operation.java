package bankaccount;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Objects;

public class Operation {
    private final OperationType type;
    private final LocalDateTime date;
    private final String accountID;
    private final BigDecimal amount;
    private final BigDecimal newBalance;

    private Operation(OperationType type, LocalDateTime date, String accountID, BigDecimal amount, BigDecimal newBalance) {
        this.type = type;
        this.date = date;
        this.accountID = accountID;
        this.amount = amount;
        this.newBalance = newBalance;
    }

    public static Operation create(OperationType type, LocalDateTime date, String accountID, BigDecimal amount, BigDecimal newBalance) {
        return new Operation(type, date, accountID, amount.setScale(2, RoundingMode.HALF_UP), newBalance.setScale(2, RoundingMode.HALF_UP));
    }

    public OperationType getType() {
        return type;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getAccountID() {
        return accountID;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getNewBalance() {
        return newBalance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Operation)) return false;
        Operation operation = (Operation) o;
        return getType() == operation.getType() && Objects.equals(getDate(), operation.getDate()) && Objects.equals(getAccountID(), operation.getAccountID()) && Objects.equals(getAmount(), operation.getAmount()) && Objects.equals(getNewBalance(), operation.getNewBalance());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType(), getDate(), getAccountID(), getAmount(), getNewBalance());
    }
}
