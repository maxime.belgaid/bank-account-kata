package bankaccount;

import bankaccount.exceptions.InvalidAccountIDException;
import bankaccount.exceptions.InvalidAmountException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@DisplayName("CheckingAccountService")
@ExtendWith(MockitoExtension.class)
public class CheckingAccountServiceTest {
    @Mock
    private OperationRepository repository;

    @Mock
    private OperationsFormatter formatter;

    @Mock
    private OperationsPrinter printer;

    private final Clock clock = Clock.fixed(Instant.EPOCH, ZoneId.of("UTC"));

    @Nested
    @DisplayName("When making a deposit")
    class DepositTest {

        @ParameterizedTest(name = "{0}")
        @ValueSource(doubles = {1, 15, 100.15, 18647898.365})
        @DisplayName("Should call the repository")
        void givenCorrectAmount_whenDepositing_shouldCallRepository(double amount) throws InvalidAccountIDException, InvalidAmountException {
            final var service = new CheckingAccountService(repository, clock);
            var inOrder = inOrder(repository);
            final var optional = Optional.of(Operation.create(OperationType.DEPOSIT, LocalDateTime.now(clock), "1", BigDecimal.valueOf(50), BigDecimal.valueOf(100)));
            final var newOperation = Operation.create(OperationType.DEPOSIT, LocalDateTime.now(clock), "1", BigDecimal.valueOf(amount), BigDecimal.valueOf(100 + amount));
            when(repository.findLast("1")).thenReturn(optional);

            service.deposit("1", BigDecimal.valueOf(amount));

            inOrder.verify(repository, times(1)).findLast("1");
            inOrder.verify(repository, times(1)).add("1", newOperation);
            verifyNoMoreInteractions(repository);
        }

        @ParameterizedTest(name = "{0}")
        @ValueSource(doubles = {0, -15, -100.15, -18647898.365})
        @DisplayName("Should throw an Exception and not call the repository when depositing an invalid amount")
        void givenInvalidAmount_whenDepositing_shouldThrowException(double amount) throws InvalidAccountIDException, InvalidAmountException {
            final var service = new CheckingAccountService(repository, clock);
            when(repository.add(any(String.class), any(Operation.class))).thenThrow(InvalidAmountException.class);

            assertThrows(InvalidAmountException.class, () -> service.deposit("1", BigDecimal.valueOf(amount)));
            verify(repository, times(1)).findLast("1");
            verifyNoMoreInteractions(repository);
        }

        @Test
        @DisplayName("Should throw an Exception and not call the repository when using an invalid accountID")
        void givenInvalidAccountID_whenDepositing_shouldThrowException() throws InvalidAccountIDException {
            final var service = new CheckingAccountService(repository, clock);
            when(repository.findLast(any(String.class))).thenThrow(InvalidAccountIDException.class);

            assertThrows(InvalidAccountIDException.class, () -> service.deposit("", BigDecimal.valueOf(1)));
            verify(repository, times(1)).findLast("");
            verifyNoMoreInteractions(repository);
        }
    }

    @Nested
    @DisplayName("When making a withdrawal")
    class WithdrawTest {

        @Disabled
        @ParameterizedTest(name = "{0}")
        @ValueSource(doubles = {1, 2, 15, 100.15})
        @DisplayName("Should call the repository")
        void givenCorrectAmount_whenWithdrawing_shouldCallRepository(double amount) throws InvalidAccountIDException, InvalidAmountException {
            final var service = new CheckingAccountService(repository, clock);
            final var lastOperation = Operation.create(OperationType.WITHDRAWAL, LocalDateTime.now(clock), "1", BigDecimal.valueOf(amount), BigDecimal.valueOf(150));
            final var newOperation = Operation.create(OperationType.WITHDRAWAL, LocalDateTime.now(clock), "1", BigDecimal.valueOf(amount), BigDecimal.valueOf(150 - amount));
            when(repository.findLast("1")).thenReturn(Optional.of(lastOperation));

            service.withdraw("1", BigDecimal.valueOf(amount));

            final var inOrder = inOrder(repository);
            inOrder.verify(repository, times(1)).findLast("1");
            inOrder.verify(repository, times(1)).add("1", newOperation);
            verifyNoMoreInteractions(repository);
        }

        @ParameterizedTest(name = "{0}")
        @ValueSource(doubles = {0, -15, -100.15, -18647898.365})
        @DisplayName("Should throw an Exception and not call the repository when the amount is invalid")
        void givenInvalidAmount_whenWithdrawing_shouldThrowException(double amount) throws InvalidAccountIDException, InvalidAmountException {
            final var service = new CheckingAccountService(repository, clock);
            when(repository.add(any(String.class), any(Operation.class))).thenThrow(InvalidAmountException.class);

            assertThrows(InvalidAmountException.class, () -> service.withdraw("1", BigDecimal.valueOf(amount)));
            verify(repository, times(1)).findLast("1");
            verifyNoMoreInteractions(repository);
        }

        @Test
        @DisplayName("Should throw an Exception and not call the repository when the accountID in invalid")
        void givenInvalidAccountID_whenWithdrawing_shouldThrowInvalidAccountIDException() throws InvalidAccountIDException {
            final var service = new CheckingAccountService(repository, clock);
            when(repository.findLast(any(String.class))).thenThrow(InvalidAccountIDException.class);

            assertThrows(InvalidAccountIDException.class, () -> service.withdraw("", BigDecimal.valueOf(1)));
            verify(repository, times(1)).findLast("");
            verifyNoMoreInteractions(repository);
        }
    }

    @Nested
    @DisplayName("When printing")
    class PrintingTest {

        @Test
        @DisplayName("Should call the printer and formatter")
        void shouldCallPrinterMethod() throws InvalidAccountIDException {
            final var service = new CheckingAccountService(repository, clock);
            final var operations = List.of(
                    Operation.create(OperationType.DEPOSIT, LocalDateTime.now(), "1", BigDecimal.valueOf(10), BigDecimal.valueOf(100)),
                    Operation.create(OperationType.WITHDRAWAL, LocalDateTime.now(), "2", BigDecimal.valueOf(5555.4879), BigDecimal.valueOf(100)));
            when(repository.findAll("1")).thenReturn(operations);
            when(formatter.format(operations)).thenReturn("formattedTable");

            service.printStatement("1", formatter, printer);

            final var inOrder = inOrder(repository, formatter, printer);
            inOrder.verify(repository, times(1)).findAll("1");
            inOrder.verify(formatter, times(1)).format(operations);
            inOrder.verify(printer, times(1)).printOperations("formattedTable");
            inOrder.verifyNoMoreInteractions();
        }
    }
}
