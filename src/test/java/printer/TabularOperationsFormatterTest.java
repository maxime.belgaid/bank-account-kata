package printer;

import bankaccount.Operation;
import bankaccount.OperationType;
import bankaccount.printer.TabularOperationsFormatter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@DisplayName("TabularOperationsFormatter")
public class TabularOperationsFormatterTest {

    @Test
    @DisplayName("Should show all operations and header")
    void shouldFormatOperationList() {
        final var formatter = new TabularOperationsFormatter();
        final var expectedResult = String.format(
                        "|                Date |           Operation |             Details |     Account Balance |%n"+
                        "|          2021/04/27 |             DEPOSIT |              10.00€ |             100.00€ |%n" +
                        "|          2021/04/27 |          WITHDRAWAL |           -5555.49€ |            -100.00€ |%n" +
                        "|          2021/04/27 |             DEPOSIT |           88888.32€ |             100.00€ |%n");

        final var result = formatter.format(List.of(
                Operation.create(OperationType.DEPOSIT, LocalDateTime.now(), "1", BigDecimal.valueOf(10), BigDecimal.valueOf(100)),
                Operation.create(OperationType.WITHDRAWAL, LocalDateTime.now(), "2", BigDecimal.valueOf(5555.4879), BigDecimal.valueOf(100)),
                Operation.create(OperationType.DEPOSIT, LocalDateTime.now(), "3", BigDecimal.valueOf(88888.32147), BigDecimal.valueOf(100))));

        assertEquals(expectedResult, result);
    }

    @Test
    @DisplayName("Should say there is no operations yet when the operations list is empty")
    void givenEmptyOperationList_shouldIndicateIt() {
        final var formatter = new TabularOperationsFormatter();
        final var expectedResult = String.format(
                        "|                Date |           Operation |             Details |     Account Balance |%n" +
                        "No operation registered yet");

        final var formattedOutput = formatter.format(List.of());

        assertEquals(expectedResult, formattedOutput);
    }
}
