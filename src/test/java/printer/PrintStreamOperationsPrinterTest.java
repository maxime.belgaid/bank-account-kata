package printer;

import bankaccount.printer.PrintStreamOperationsPrinter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.PrintStream;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("PrintStreamOperationsPrinter")
public class PrintStreamOperationsPrinterTest {

    @Mock
    private PrintStream stream;

    @Test
    @DisplayName("Should call the PrintStream")
    void shouldCallTheFormatterMethod() {
        final var printer = new PrintStreamOperationsPrinter(stream);

        printer.printOperations("Hello");

        verify(stream, times(1)).println("Hello");
        verifyNoMoreInteractions(stream);
    }
}
